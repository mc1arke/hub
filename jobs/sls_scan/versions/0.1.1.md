* Enable `artifact:expose_as` option to display job result in merge request
* Add new variable `OUTPUT_PATH` which allows to specify a custom output path
